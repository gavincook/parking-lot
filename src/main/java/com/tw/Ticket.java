package com.tw;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class Ticket {
    private String carNum;

    private ParkingLot parkingLot;

    public Ticket(String carNum, ParkingLot parkingLot) {
        this.carNum = carNum;
        this.parkingLot = parkingLot;
    }

    public ParkingLot getParkingLot() {
        return this.parkingLot;
    }
}
