package com.tw;

import java.util.Arrays;
import java.util.Optional;

public class ParkingBoy {

    private ParkingLot[] parkingLots;

    public ParkingBoy(ParkingLot... parkingLots) {
        this.parkingLots = parkingLots;
    }

    public Ticket park(Car car) {
        Optional<ParkingLot> availableParkingLot = Arrays.stream(parkingLots).filter(
                parkingLot -> !parkingLot.isFullPacked()).findFirst();
        return availableParkingLot.map(parkingLot -> parkingLot.park(car)).orElseThrow(
                NoParkingLotAvaiableException::new);
    }
}
