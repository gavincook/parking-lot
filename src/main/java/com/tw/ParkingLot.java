package com.tw;

import java.util.HashMap;
import java.util.Map;

public class ParkingLot {

    private int capacity;

    private int parkedNum;

    private Map<Ticket, Car> parked = new HashMap<>();

    public ParkingLot(int capacity) {
        this.capacity = capacity;
        this.parkedNum = 0;
    }

    public Ticket park(Car car) {
        if (parkedNum >= capacity) {
            throw new ParkingSpaceNotEnoughException();
        }
        parkedNum++;
        Ticket ticket = new Ticket(car.getCarNum(), this);
        parked.put(ticket, car);
        return ticket;
    }

    public Car pickUp(Ticket ticket) {
        if (parked.containsKey(ticket)) {
            Car car = parked.get(ticket);
            parked.remove(ticket);
            return car;
        }
        throw new CarNotFoundException();
    }

    public boolean isFullPacked() {
        return parkedNum >= capacity;
    }
}
