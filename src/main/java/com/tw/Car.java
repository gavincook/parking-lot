package com.tw;

import lombok.Getter;

@Getter
public class Car {
    private String carNum;

    public Car(String carNum) {
        this.carNum = carNum;
    }
}
