package com.tw;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class ParkingBoyTest {

    @Test
    public void should_get_ticket_when_parking_1_car_by_parking_boy_with_available_parking_lot() {
        ParkingLot parkingLot = new ParkingLot(1);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        Car car = new Car("川AE0000");
        Ticket ticket = parkingBoy.park(car);

        assertThat(ticket, notNullValue());
    }

    @Test
    public void should_parked_in_first_parking_lot_when_parking_multiple_cars_with_first_lot_space_enough() {
        ParkingLot firstParkingLot = new ParkingLot(2);
        ParkingLot secondParkingLot = new ParkingLot(2);
        ParkingBoy parkingBoy = new ParkingBoy(firstParkingLot, secondParkingLot);
        Car sixsixsixCar = new Car("川A66666");
        Car fafafaCar = new Car("川A88888");

        Ticket sixsixsixTicket = parkingBoy.park(sixsixsixCar);
        Ticket fafafaTicket = parkingBoy.park(fafafaCar);

        assertThat(sixsixsixTicket.getParkingLot(), is(firstParkingLot));
        assertThat(fafafaTicket.getParkingLot(), is(firstParkingLot));
    }

    @Test
    public void should_park_in_multiple_parking_lot_ordered_when_parking_multiple_cars_with_low_space_free() {
        ParkingLot firstParkingLot = new ParkingLot(2);
        ParkingLot secondParkingLot = new ParkingLot(2);
        ParkingBoy parkingBoy = new ParkingBoy(firstParkingLot, secondParkingLot);
        Car firstCar = new Car("川A11111");
        Car secondCar = new Car("川A22222");
        Car thirdCar = new Car("川A33333");
        Car fourthCar = new Car("川A44444");

        Ticket firstTicket = parkingBoy.park(firstCar);
        Ticket secondTicket = parkingBoy.park(secondCar);
        Ticket thirdTicket = parkingBoy.park(thirdCar);
        Ticket fourthTicket = parkingBoy.park(fourthCar);

        assertThat(firstTicket.getParkingLot(), is(firstParkingLot));
        assertThat(secondTicket.getParkingLot(), is(firstParkingLot));
        assertThat(thirdTicket.getParkingLot(), is(secondParkingLot));
        assertThat(fourthTicket.getParkingLot(), is(secondParkingLot));
    }
}
