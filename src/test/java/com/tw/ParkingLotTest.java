package com.tw;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class ParkingLotTest {

    @Test
    public void should_get_ticket_when_parking_1_car_in_available_parking_lot() {
        ParkingLot parkingLot = new ParkingLot(1);
        Car car = new Car("川AE0000");
        Ticket ticket = parkingLot.park(car);
        assertThat(ticket, notNullValue());
    }


    @Test
    public void should_get_different_tickets_when_parking_different_cars_in_available_parking_lot() {
        ParkingLot parkingLot = new ParkingLot(2);
        Car sixsixsixCar = new Car("川A66666");
        Ticket sixsxisixTicket = parkingLot.park(sixsixsixCar);

        Car fafafaCar = new Car("川A88888");
        Ticket fafafaTicket = parkingLot.park(fafafaCar);

        assertThat(fafafaTicket, not(sixsxisixTicket));
    }

    @Test(expected = ParkingSpaceNotEnoughException.class)
    public void should_throw_ParkingSpaceNotEnoughException_when_parking_car_in_full_parked_parking_lot() {
        ParkingLot parkingLot = getFullParkedParkingLot();

        Car fafafaCar = new Car("川A88888");
        parkingLot.park(fafafaCar);
    }

    @Test
    public void should_get_car_when_pick_up_car_from_only_the_car_parked_parking_lot() {
        ParkingLot parkingLot = new ParkingLot(8);
        Car sixsixsixCar = new Car("川A66666");
        Ticket ticket = parkingLot.park(sixsixsixCar);
        Car pickedUpCar = parkingLot.pickUp(ticket);
        assertThat(pickedUpCar, is(sixsixsixCar));
    }

    @Test
    public void should_get_car_when_pick_up_car_from_the_car_and_other_cars_parked_parking_lot() {
        ParkingLot parkingLot = new ParkingLot(8);
        Car sixsixsixCar = new Car("川A66666");
        Ticket sixsixsixTicket = parkingLot.park(sixsixsixCar);

        Car fafafaCar = new Car("川A88888");
        Ticket fafafaTicket = parkingLot.park(fafafaCar);

        Car pickedUpCar = parkingLot.pickUp(sixsixsixTicket);
        assertThat(pickedUpCar, is(sixsixsixCar));

        pickedUpCar = parkingLot.pickUp(fafafaTicket);
        assertThat(pickedUpCar, is(fafafaCar));
    }

    @Test(expected = CarNotFoundException.class)
    public void should_throw_CarNotFoundException_when_pick_up_car_twice_from_the_car_parked_parking_lot() {
        ParkingLot parkingLot = new ParkingLot(8);
        Car sixsixsixCar = new Car("川A66666");
        Ticket sixsixsixTicket = parkingLot.park(sixsixsixCar);

        Car pickUpCar = parkingLot.pickUp(sixsixsixTicket);
        assertThat(pickUpCar, is(sixsixsixCar));

        parkingLot.pickUp(sixsixsixTicket);
    }

    private ParkingLot getFullParkedParkingLot() {
        ParkingLot parkingLot = new ParkingLot(1);
        Car sixsixsixCar = new Car("川A66666");
        parkingLot.park(sixsixsixCar);
        return parkingLot;
    }
}
